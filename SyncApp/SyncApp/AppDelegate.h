//
//  AppDelegate.h
//  SyncApp
//
//  Created by shupeng on 14-8-12.
//  Copyright (c) 2014年 CoSciCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
