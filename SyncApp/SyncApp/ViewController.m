//
//  ViewController.m
//  SyncApp
//
//  Created by shupeng on 14-8-12.
//  Copyright (c) 2014年 CoSciCo. All rights reserved.
//

#import "ViewController.h"
#import "LKSyncManager.h"


typedef NS_ENUM(NSInteger, state)
{
    STATE_IDLE,
    STATE_UPLOADING,
    STATE_DOWNLOADING,
};


@interface ViewController () <UITextViewDelegate>
{
    UIActivityIndicatorView *_indicatorView;
    UIBarButtonItem *_indicatorItem;
    UITextView *_textView;
    
    UITextView *_logView;
    
    LKSyncManager *syncMgr;
}
@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    syncMgr = [LKSyncManager sharedManager];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.title = @"SyncApp";
    
    UIBarButtonItem *syncBtn = [[UIBarButtonItem alloc] initWithTitle:@"Sync" style:UIBarButtonItemStylePlain target:self action:@selector(syncBtnPressed:)];
    self.navigationItem.rightBarButtonItem = syncBtn;
    UIBarButtonItem *dismissBtn = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBtnPressed:)];
    self.navigationItem.leftBarButtonItem = dismissBtn;
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_indicatorView stopAnimating];
    _indicatorItem = [[UIBarButtonItem alloc] initWithCustomView:_indicatorView];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 20 + 44 + 20, 300, 160)];
    _textView.layer.masksToBounds = YES;
    _textView.layer.cornerRadius = 5.f;
    _textView.backgroundColor = [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:.9f];
    _textView.returnKeyType = UIReturnKeyDone;
    _textView.delegate = self;
    [self.view addSubview:_textView];
    
    NSString *content = [syncMgr localNoteContent];
    if (content.length < 1) {
        [syncMgr syncFromSeverImmediately];
    } else {
        _textView.text = content;
    }
    
    _logView = [[UITextView alloc] initWithFrame:CGRectMake(0, _textView.frame.origin.y + _textView.frame.size.height + 3, 320, self.view.frame.size.height - (_textView.frame.origin.y + _textView.frame.size.height + 3))];
    _logView.editable = NO;
    _logView.attributedText = [[NSAttributedString alloc] initWithString:@"log start:\n" attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
    [self.view addSubview:_logView];
    
    
    
    [syncMgr registerContentSyncFromServerTask:^(NSString *content) {
        _textView.text = content;
    }];
    
    [self performSelector:SELE(startUpSyncPeroidically) withObject:nil afterDelay:10];
    [self performSelector:SELE(startDownSyncPeroidically) withObject:nil afterDelay:20];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Sync
- (void)startUpSyncPeroidically {
    [syncMgr syncToSeverWithContent:_textView.text];
    
//    [self performSelector:SELE(startUpSyncPeroidically) withObject:nil afterDelay:30];
}

- (void)startDownSyncPeroidically {
    [syncMgr syncFromSeverImmediately];
    
    [self performSelector:SELE(startDownSyncPeroidically) withObject:nil afterDelay:30];
}


#pragma mark - Local Function
- (void)appendingLog:(NSString *)log
{
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc] init];
    [dateFomatter setDateFormat:@"HH:mm:ss:SSS"];
    NSString *date = [dateFomatter stringFromDate:[NSDate date]];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithAttributedString:_logView.attributedText];
    [attr appendAttributedString:[[NSAttributedString alloc] initWithString:[date stringByAppendingFormat:@"     %@\n", log] attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}]];
    _logView.attributedText = attr;
    
    [_logView scrollRangeToVisible:NSMakeRange(_logView.text.length - 2, 1)];
}

#pragma mark - UIView Event
- (void)syncBtnPressed:(UIBarButtonItem *)sender
{
    [syncMgr syncFromSeverImmediately];
}

- (void)dismissBtnPressed:(UIBarButtonItem *)sender
{
    [_textView resignFirstResponder];
}

#pragma mark - TextView Delegate


@end
