//
//  CRKeyboardUtility.m
//  EmdeonIPadEMR
//
//  Created by Peter on 11/13/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//

#import "CRKeyboardUtility.h"
#import "CRMacros.h"

@interface CRKeyboardManager : NSObject
{
    BOOL _keyboardVisible;
}
@property (nonatomic, readonly, getter = isKeyboardVisible) BOOL keyboardVisible;
+ (id)sharedKeyboardManager;
@end

@implementation CRKeyboardManager
+ (id)sharedKeyboardManager {
    __strong static CRKeyboardManager *sharedKeyboardMgr = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedKeyboardMgr = [CRKeyboardManager new];
    });
    
    return sharedKeyboardMgr;
}

- (id)init {
    if (self=[super init]) {
        CRRegisterNotification(keyboardDidShow, UIKeyboardDidShowNotification);
        CRRegisterNotification(keyboardDidHide, UIKeyboardDidHideNotification);
    }
    return self;
}

- (BOOL)isKeyboardVisible {
    return _keyboardVisible;
}

- (void)keyboardDidShow {
    _keyboardVisible = YES;
}

- (void)keyboardDidHide {
    _keyboardVisible = NO;
}


@end



@implementation CRKeyboardUtility
+ (BOOL)startObserving {
    id keyboardMgr = [CRKeyboardManager sharedKeyboardManager];
    if (keyboardMgr) {
        return YES;
    }
    return NO;
}

+ (BOOL)isKeyboardVisible {
    CRKeyboardManager *keyboardMgr = [CRKeyboardManager sharedKeyboardManager];
    return keyboardMgr.keyboardVisible;
}


@end
