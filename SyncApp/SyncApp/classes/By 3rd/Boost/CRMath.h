//
//  CRMath.h
//  CRBoost
//
//  Created by Peter on 9/13/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//

#ifndef CRMath_h
#define CRMath_h

#import <sys/sysctl.h>
#import <sys/xattr.h>
#import <AVFoundation/AVFoundation.h>
#import "CRMacros.h"

/**
 * date formatter
 *
 * date format
 * 0: no / or - between numbers
 * 1: /
 * 2: -
 *
 * time format
 * 0: :
 */
static NSString *const kDateTemplate0yyyyMMdd = @"yyyyMMdd";
static NSString *const kDateTemplate1MMddyyyy = @"MM/dd/yyyy";
static NSString *const kDateTemplate1MMddyy = @"MM/dd/yy";
static NSString *const kDateTemplate1ddMMyyyy0HHmmss = @"dd/MM/yyyy HH:mm:ss";

static NSString *const kDateTemplate2MMddyyyy = @"MM-dd-yyyy";
static NSString *const kDateTemplate2yyyyMMdd0HHmmss = @"yyyy-MM-dd HH:mm:ss";
static NSString *const kDateTemplate2yyyyMMdd0HHmmssZZZ = @"yyyy-MM-dd HH:mm:ss ZZZ";

static NSString *const kDateTemplate0hmma = @"h:mm a";


#pragma mark -
#pragma mark angle
//==================angle==================
#define CRRadian2degrees(degrees)    (M_PI * (degrees) / 180.0)
#define CRDegrees2radian(radian)     ((radian) * 180.0 / M_PI)


#define CREven(num)    (((int)(num) & 0x01) ? ((int)(num)-1) : (int)(num))
#define CREven_2(num)  (CREven(num) / 2)


CG_INLINE CGFloat //in radians
CRArcAngle(CGPoint start, CGPoint end) {
    CGPoint originPoint = CGPointMake(end.x - start.x, start.y - end.y);
    float radians = atan2f(originPoint.y, originPoint.x);

    radians = radians < 0.0 ? (M_PI*2 + radians) : radians;

    NSLog(@"arc radians is %f", radians);

    return M_PI*2 - radians;
}

CG_INLINE CGFloat //in radians
CRDistance(CGPoint start, CGPoint end) {
    float originX = end.x - start.x;
    float originY = end.y - start.y;

    return sqrt(originX*originX + originY*originY);
}

CG_INLINE CGPoint
CRCenterPoint(CGPoint start, CGPoint end) {
    return CGPointMake((end.x + start.x)/2, (end.y + start.y)/2);
}


#pragma mark -
#pragma mark graphics
//==================point==================
CG_INLINE CGPoint
CRPointSubtract(CGPoint p1, CGPoint p2) {
    return CGPointMake(p1.x - p2.x, p1.y - p2.y);
}

CG_INLINE CGPoint
CRFrameCenter(CGRect rect) {
    return CGPointMake(CREven_2(rect.origin.x + rect.size.width),
                       CREven_2(rect.origin.y - rect.size.height));
}

CG_INLINE CGPoint
CRBoundCenter(CGRect rect) {
    //round version
    return CGPointMake(CREven_2(rect.size.width), CREven_2(rect.size.height));
//    return CGPointMake(frame.size.width/2, frame.size.height/2);
}

//==================rect==================
CG_INLINE CGRect
CRCenteredFrame(CGRect frame, CGPoint center) {
    frame.origin = CGPointMake((int)(center.x-frame.size.width/2),
                               (int)(center.y+frame.size.height/2));
    return frame;
}

CG_INLINE CGRect
CRRectAddHeight(CGRect rect, CGFloat height) {
    return (CGRect){rect.origin, {rect.size.width, rect.size.height + height}};
}

CG_INLINE CGRect
CRRectUpdateWidth(CGRect rect, CGFloat width) {
    return (CGRect){rect.origin, {width, rect.size.height}};
}

//==================size==================
CG_INLINE CGSize
CRSizeEnlarge(CGSize size, CGFloat width, CGFloat height) {
    return CGSizeMake(size.width + width, size.height + height);
}

CG_INLINE CGSize
CRSizeZoom(CGSize size, CGFloat factor) {
    return CGSizeMake(size.width*factor, size.height*factor);
}

CG_INLINE CGSize
CRSizeAddWidth(CGSize size, CGFloat width) {
    return CRSizeEnlarge(size, width, 0);
}

CG_INLINE CGSize
CRSizeAddHeight(CGSize size, CGFloat height) {
    return CRSizeEnlarge(size, 0, height);
}

CG_INLINE CGSize
CRSizeUpdateWidth(CGSize size, CGFloat width) {
    return CGSizeMake(width, size.height);
}

CG_INLINE CGSize
CRSizeUpdateHeight(CGSize size, CGFloat height) {
    return CGSizeMake(size.width, height);
}

CG_INLINE BOOL
CRSizeLarger(CGSize s1, CGSize s2) {
    return s1.width>s2.width && s1.height>s2.height;
}


#pragma mark -
#pragma mark device
CG_INLINE CGRect
CRScreenBounds(BOOL landscape) {
    CGRect rect = [[UIScreen mainScreen] bounds];
    if (landscape && rect.size.width<rect.size.height) {
        CGFloat height = rect.size.height;
        rect.size.height = rect.size.width;
        rect.size.width = height;
    }

    return rect;
}

CG_INLINE UIWindow *
CRMainWindow(void) {
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];

    if (mainWindow == nil) {
        NSArray *arrWindow = [[UIApplication sharedApplication] windows];

        if(arrWindow.count>0) mainWindow = arrWindow[0];
    }

    return mainWindow;
}

CG_INLINE BOOL
CRKeyboardHide(void) {
    UIWindow *window = CRMainWindow();
    return [window endEditing:YES];
}


CG_INLINE CGFloat
CRSystemVolume(void) {
    return [[AVAudioSession sharedInstance] outputVolume];
}

CG_INLINE BOOL
CRIsRetina(void) {
    if (([UIScreen mainScreen].scale==2.0) && [[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)]) {
        return YES;
    } else return NO;
}

CG_INLINE UIInterfaceOrientation
CRDeviceOrientation(void) {
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsValidInterfaceOrientation(orientation)) {
        return (UIInterfaceOrientation)orientation;
    }

//    UIWindow *window = CRMainWindow();
//    window.rootViewController.interfaceOrientation;

    return [UIApplication sharedApplication].statusBarOrientation;
}


#pragma mark -
#pragma mark file
CG_INLINE NSNumber *
CRFileSize(NSString *path) {
    NSFileManager *fileMgr = [NSFileManager new]; //thread safe
    NSDictionary *attributes = [fileMgr attributesOfItemAtPath:path error:nil];
    if(attributes) return attributes[NSFileSize];
    else return nil;
}

#pragma mark -
#pragma mark url
CG_INLINE NSURL *
CRURL(NSString *url) {
    return [NSURL URLWithString:url];
}

#pragma mark -
#pragma mark UI
CG_INLINE void
CRPresentView(UIView *view, BOOL animated) {
    UIViewController *root = CRMainWindow().rootViewController;
    if (animated) {
        view.alpha = 0.0;

        [root.view addSubview:view];
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut
                         animations:^{view.alpha = 1.0;} completion:nil];
    } else {
        [root.view addSubview:view];
    }
}

CG_INLINE void
CRPresentAlert(NSString *title, NSString *msg, id delegate, NSString *canel, NSString *action) {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:delegate
                                              cancelButtonTitle:canel
                                              otherButtonTitles:action, nil];

    [alertView show];
}

CG_INLINE void
CRPresentAlert2(NSString *title, NSString *msg) {
    CRPresentAlert(title, msg, nil, @"OK", nil);
}


#pragma mark -
#pragma mark foundation
//NSDate
CG_INLINE NSDate *
CRDateInLastYear(void) {
    NSDate *today = [NSDate date];
    NSTimeInterval interval = arc4random_uniform(60 * 60 * 24 * 360);
    NSDate *date = [today dateByAddingTimeInterval:-interval];

    return date;
}

CG_INLINE NSString *
CRDateString(NSDate *date, NSString *tmplate) {
    if (!date || !tmplate) {
        return nil;
    }

    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//    NSString *dateString = [NSDateFormatter dateFormatFromTemplate:tmplate options:0 locale:usLocale];
    dateFormatter.dateFormat = tmplate;
    return [dateFormatter stringFromDate:date];
}

CG_INLINE NSDate *
CRDateTemplated(NSString *date, NSString *tmplate) {
    if (!date || !tmplate) {
        return nil;
    }
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }

    dateFormatter.dateFormat = tmplate;
    return [dateFormatter dateFromString:date];
}

CG_INLINE BOOL
CRIsSameDay(NSDate *date1, NSDate *date2) {

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp1 = [calendar components:CRCOMPS_DATE fromDate: date1];
    NSDateComponents *comp2 = [calendar components:CRCOMPS_DATE fromDate: date2];

    return comp1.year==comp2.year && comp1.month==comp2.month && comp1.day==comp2.day;
}


//NSArray
CG_INLINE NSMutableArray *
CRMArrayNull(NSInteger num) {
    NSMutableArray *array = [NSMutableArray array];
    for (int i=0; i < num; i++) {
        [array addObject:CRNull];
    }
    return array;
}

CG_INLINE id
CRArrayObject(NSArray *array, NSInteger idx) {
    if (idx < 0 || idx >= array.count) {
        return nil;
    } else return array[idx];
}


//NSMutableDictionary
CG_INLINE NSMutableDictionary *
CRMDictionaryA(NSArray *key, NSArray *value) {
    return [NSMutableDictionary dictionaryWithObjects:value forKeys:key];
}

CG_INLINE NSMutableDictionary *
CRMDictionaryD(NSDictionary *dic) {
    return [NSMutableDictionary dictionaryWithDictionary:dic];
}


//NSTimer
CG_INLINE void
CRStopTimer(__strong NSTimer **tmr) {
    NSTimer *timer = *tmr;
    if (timer && [timer isValid]) {
        [timer invalidate];
        timer = nil;
        *tmr = nil;
    }
}


//number formater
CG_INLINE NSString *
CRDigitString(NSInteger number, int padding) {
    NSString *formater = CRString(@"%%0%id", padding);
    return CRString(formater, number);
}





#endif
