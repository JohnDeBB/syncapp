//
//  NSObject+CRBoost.h
//  ECO
//
//  Created by Peter on 12/27/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CRBoost)
- (void)tryMethod:(SEL)sel;
- (void)tryMethod:(SEL)sel arg:(id)arg;
- (void)tryMethod:(SEL)sel arg:(id)arg1 arg:(id)arg2;
@end





extern NSString *const kPathFlagRetina;
extern NSString *const kPathFlagBig;
extern NSString *const kPathFlagHighlighted;

@interface NSString (CRBoost)
- (NSAttributedString *)underlineAttributeString;
//appending to file name
- (NSString *)pathByAppendingFlag:(NSString *)flag;
- (NSString *)join:(NSString *)path;
- (NSString *)joinPath:(NSString *)path;
- (NSString *)joinPath:(NSString *)path1 path:(NSString *)path2;
- (BOOL)endWith:(NSString *)string;
@end





@interface NSDate (CRBoost)
+ (NSDate *)dateWithinYear;
@end





