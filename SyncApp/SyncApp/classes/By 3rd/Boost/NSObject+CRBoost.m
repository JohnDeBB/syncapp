//
//  NSObject+CRBoost.m
//  ECO
//
//  Created by Peter on 12/27/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//

#import "NSObject+CRBoost.h"
#import "CRMacros.h"

@implementation NSObject (CRBoost)
- (void)tryMethod:(SEL)sel {
    if ([self respondsToSelector:sel]) {
        IMP imp = [self methodForSelector:sel];
        void (* func)(id, SEL) = (void *)imp;
        func(self, sel);
    }
}

- (void)tryMethod:(SEL)sel arg:(id)arg {
    if ([self respondsToSelector:sel]) {
        IMP imp = [self methodForSelector:sel];
        void (* func)(id, SEL, id) = (void *)imp;
        func(self, sel, arg);
    }
}

- (void)tryMethod:(SEL)sel arg:(id)arg1 arg:(id)arg2 {
    if ([self respondsToSelector:sel]) {
        IMP imp = [self methodForSelector:sel];
        void (* func)(id, SEL, id, id) = (void *)imp;
        func(self, sel, arg1, arg2);
    }
    
    //http://stackoverflow.com/questions/12454408/variable-number-of-method-parameters-in-objective-c-need-an-example
    
    
    //http://stackoverflow.com/questions/7017281/performselector-may-cause-a-leak-because-its-selector-is-unknown
    
    
    /*
     In your project Build Settings, under Other Warning Flags (WARNING_CFLAGS), add
     -Wno-arc-performSelector-leaks
     */
}
@end



#pragma mark -
#pragma mark NSString

NSString *const kPathFlagRetina = @"@2x";
NSString *const kPathFlagBig = @"Big";
NSString *const kPathFlagHighlighted = @"Hlt";

@implementation NSString (CRBoost)
- (NSAttributedString *)underlineAttributeString {
    static NSDictionary *underlineAttribute = nil;
    if (!underlineAttribute) {
        underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    }
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self attributes:underlineAttribute];
    
    return attributedString;
}

- (NSString *)pathByAppendingFlag:(NSString *)flag {
    NSString *extension = [self pathExtension];
    NSString *pathBody = [self stringByDeletingPathExtension];
    NSString *newPath = CRString(@"%@%@.%@", pathBody, flag, extension);
    
    return newPath;
}

- (NSString *)join:(NSString *)path {
    return [self stringByAppendingString:path];
}

- (NSString *)joinPath:(NSString *)path {
    return [self stringByAppendingPathComponent:path];
}

- (NSString *)joinPath:(NSString *)path1 path:(NSString *)path2 {
    return [[self joinPath:path1] joinPath:path2];
}

- (BOOL)endWith:(NSString *)string {
    NSRange range = [self rangeOfString:string];
    if (range.location > 0 && self.length==range.location+range.length)
        return YES;
    else return NO;
}
@end





#pragma mark -
#pragma mark NSDate
@implementation NSDate (CRBoost)
+ (NSDate *)dateWithinYear {
    NSDate *today = [NSDate date];
    NSTimeInterval interval = arc4random_uniform(60 * 60 * 24 * 360);
    
    NSDate *date = [today dateByAddingTimeInterval:-interval];
    
    return date;
}
@end





