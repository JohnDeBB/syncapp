//
//  UIColor+CRBoost.h
//  TED
//
//  Created by Peter on 2/18/14.
//  Copyright (c) 2014 Cocoa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CRBoost)
- (UIColor *)lighten:(float)amount; //amount: 0-1
- (UIColor *)darken:(float)amount;

+ (UIColor *)randomColor;
+ (UIColor *)randomColorMix:(UIColor *)color;
@end