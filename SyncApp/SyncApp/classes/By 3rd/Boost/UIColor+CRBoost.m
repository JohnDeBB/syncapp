//
//  UIColor+CRBoost.m
//  TED
//
//  Created by Peter on 2/18/14.
//  Copyright (c) 2014 Cocoa. All rights reserved.
//

#import "UIColor+CRBoost.h"
#import "CRMacros.h"

#pragma mark -
#pragma mark UIColor
@implementation UIColor (CRBoost)
- (UIColor *)lighten:(float)amount {
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a]) {
        float newB = b * (1 + amount);
        return [UIColor colorWithHue:h saturation:s brightness:newB alpha:a];
    }
    
    return self;
}

- (UIColor *)darken:(float)amount {
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a]) {
        float newB = b * (1 - amount);
        return [UIColor colorWithHue:h saturation:s brightness:newB alpha:a];
    }
    
    return self;
}

+ (UIColor *)randomColor {
    int red = arc4random_uniform(256);
    int green = arc4random_uniform(256);
    int blue = arc4random_uniform(256);
    return CRRGB(red, green, blue);
}

+ (UIColor *)randomColorMix:(UIColor *)color {
    UIColor *originalColor = [self randomColor];
    
    CGFloat red, green, blue;
    [originalColor getRed:&red green:&green blue:&blue alpha:nil];
    
    CGFloat mixRed, mixGreen, mixBlue;
    [color getRed:&mixRed green:&mixGreen blue:&mixBlue alpha:nil];
    
    return CRRGB_F((red+mixRed)/2, (green+mixGreen)/2, (blue+mixBlue)/2);
}

@end
