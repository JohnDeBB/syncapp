//
//  Utility.h
//  CRBoost
//
//  Created by Peter on 9/13/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//





@interface Utility : NSObject

// directory
+ (NSString *)documentDirectory;
+ (NSString *)libraryDirectory;
+ (NSString *)cachesDirectory;
+ (NSString *)tempDirectory;
+ (BOOL)ensureExistsOfDirectory:(NSString *)dirPath;


// path
+ (NSString *)pathOfResourceFile:(NSString *)path;


// GCD
+ (void)performeBackgroundTask:(void (^)(void))backgroundBlock beforeMainTask:(void(^)(void))mainBlock;
+ (void)performePostponed:(NSTimeInterval)delay task:(void (^)(void))task;


// view
+ (void)presentView:(UIView *)view animated:(BOOL)animated;

+ (UIView *)viewByRoundingAndStrokingImage:(UIImage *)image;


//interaction
+ (void)lockUserInteraction;
+ (void)unlockUserInteraction;
+ (void)lockUserInteractionWithDuration:(NSTimeInterval)duration;


// image
+ (UIImage *)imageByScalingImage:(UIImage *)image toSize:(CGSize)newSize;
+ (UIImage *)imageByColorizingImage:(UIImage *)image withColor:(UIColor *)color;
+ (UIImage *)imageByRenderingImage:(UIImage *)image withColor:(UIColor *)color;



// animation


// system


// device


// story board
+ (id)controllerWithIdentifier:(NSString *)identifier;

// xib name must be the same as the class name
+ (id)controllerWithNib:(NSString *)nib;

// activity
//+ (void)presentHud:(NSString *)greeting;
//+ (void)dismissHud;


// navigation
+ (void)gotoURL:(NSString *)str;


@end
