//
//  Utility.m
//  CRBoost
//
//  Created by Peter on 9/13/13.
//  Copyright (c) 2013 Cocoa. All rights reserved.
//

#import "Utility.h"
#import "CRMacros.h"
#import "CRMath.h"
#import <QuartzCore/QuartzCore.h>

@implementation Utility

#pragma mark -
#pragma mark directory
//Use this directory to store critical user documents and app data files.
+ (NSString *)documentDirectory {
    NSArray *arrDocument = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    return arrDocument[0];
}

//This directory is the top-level directory for files that are not user data files.
+ (NSString *)libraryDirectory {
    NSArray *arrLibrary = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    return arrLibrary[0];
}

+ (NSString *)cachesDirectory {
    NSArray *arrCache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = arrCache[0];
    
    [self ensureExistsOfDirectory:cachePath];
    return cachePath;
}

+ (NSString *)tempDirectory {
    NSString *tempPath = NSTemporaryDirectory();
    
    [self ensureExistsOfDirectory:tempPath];
    return tempPath;
}

+ (BOOL)ensureExistsOfDirectory:(NSString *)dirPath {
    BOOL isDir;
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if (![fileMgr fileExistsAtPath:dirPath isDirectory:&isDir] || !isDir) {
        BOOL succeed = [fileMgr createDirectoryAtPath:dirPath
                          withIntermediateDirectories:YES
                                           attributes:nil
                                                error:nil];
        return succeed;
    } else return YES;
}

#pragma mark -
#pragma mark path
+ (NSString *)bundlePath {
    return [[NSBundle mainBundle] resourcePath];
}

+ (NSString *)pathOfResourceFile:(NSString *)path {
    return [[self bundlePath] stringByAppendingPathComponent:path];
}



#pragma mark -
#pragma mark file


#pragma mark -
#pragma mark GCD
+ (void)performeBackgroundTask:(void (^)(void))backgroundBlock beforeMainTask:(void (^)(void))mainBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        backgroundBlock();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            mainBlock();
        });
    });
}

+ (void)performePostponed:(NSTimeInterval)delay task:(void (^)(void))task {
    //delay: time in second
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
    dispatch_after(delayTime, dispatch_get_main_queue(), ^(void){
        task();
    });
}

#pragma mark -
#pragma mark view
+ (void)presentView:(UIView *)view animated:(BOOL)animated {
    UIViewController *root = CRMainWindow().rootViewController;
    if (animated) {
        view.alpha = 0.0;
        
        [root.view addSubview:view];
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut
                         animations:^{view.alpha = 1.0;} completion:nil];
    } else {
        [root.view addSubview:view];
    }
}

/*
+ (UIView *)viewWithColor:(UIColor *)color size:(CGSize)size {
    UIView *view = [[UIView alloc] initWithFrame:(CGRect){0, 0, size}];
    view.backgroundColor = color;
    
    return view;
}*/

+ (UIView *)viewByRoundingAndStrokingImage:(UIImage *)image {
    // make new layer to contain shadow and masked image
    CGRect rect = (CGRect){0, 0, image.size};
    CALayer* containerLayer = [CALayer layer];
    containerLayer.shadowColor = [UIColor blackColor].CGColor;
    containerLayer.shadowRadius = 2.5f;
    containerLayer.shadowOffset = CGSizeMake(0.f, 1.f);
    containerLayer.shadowOpacity = 0.8;
    
    // use the image to mask the image into a circle
    CALayer *contentLayer = [CALayer layer];
    contentLayer.contents = (id)image.CGImage;
    contentLayer.frame = rect;
    
    contentLayer.borderColor = [UIColor colorWithRed:0.825 green:0.82 blue:0.815 alpha:1.0].CGColor;
    contentLayer.borderWidth = 1.0;
    contentLayer.cornerRadius = 6.0;
    contentLayer.masksToBounds = YES;
    
    // add masked image layer into container layer so that it's shadowed
    [containerLayer addSublayer:contentLayer];
    
    // add container including masked image and shadow into view
    UIView *view = [[UIView alloc] initWithFrame:rect];
    [view.layer addSublayer:contentLayer];
    
    return view;
}


#pragma mark -
#pragma mark interaction
+ (void)lockUserInteraction {
//    DLOG(@"time to start lock user screen.....");
    
    UIWindow *window = CRMainWindow();
    
    [window setUserInteractionEnabled:NO];
}

+ (void)unlockUserInteraction {
//    DLOG(@"time to end lock user screen.....");
    
    UIWindow *window = CRMainWindow();
    
    [window setUserInteractionEnabled:YES];
}

+ (void)lockUserInteractionWithDuration:(NSTimeInterval)duration {
    [self lockUserInteraction];
    
    [self performePostponed:duration task:^{
        [self unlockUserInteraction];
    }];
    
    /*
    [NSTimer scheduledTimerWithTimeInterval:duration
                                     target:self
                                   selector:SELE(unlockUserInteraction)
                                   userInfo:nil
                                    repeats:NO];*/
}


#pragma mark -
#pragma mark image
+ (UIImage *)imageByScalingImage:(UIImage *)image toSize:(CGSize)newSize {
    //image.scale or [UIScreen mainScreen].scale)
    //create a graphics image context
    //used to be: UIGraphicsBeginImageContext(newSize);
	UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale); 
    
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)]; //draw in context
	
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext(); //new image
	
	UIGraphicsEndImageContext();
	
	return newImage;
}

+ (UIImage *)imageByColorizingImage:(UIImage *)image withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = (CGRect){0, 0, image.size};
    
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -area.size.height);
    
    CGContextSaveGState(context);
    CGContextClipToMask(context, area, image.CGImage);
    
    [color set];
    
    CGContextFillRect(context, area);
    CGContextRestoreGState(context);
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    CGContextDrawImage(context, area, image.CGImage);
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}

+ (UIImage *)imageByRenderingImage:(UIImage *)image withColor:(UIColor *)color {
    //decode color
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    CGColorRef colorRef = color.CGColor;
    size_t numComponents = CGColorGetNumberOfComponents(colorRef);
    const CGFloat *colors = CGColorGetComponents(colorRef);
    if (numComponents == 2) {
        red = green = blue = colors[0];
        alpha = colors[1];
    } else if (numComponents == 4) {
        red = colors[0];
        green = colors[1];
        blue = colors[2];
        alpha = colors[3];
    }
    
    //decode image
    CGImageRef imageRef = image.CGImage;
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(width * height * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPercomponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPercomponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, (CGRect){0, 0, width,height}, imageRef);
    CGContextRelease(context);
    
    // change color
    int byteIndex = 0;
    for (int ii=0; ii < width*height; ii++) {
        rawData[byteIndex] = (char)(red * 255);
        rawData[byteIndex+1] = (char)(green * 255);
        rawData[byteIndex+2] = (char)(blue * 255);
        //        if(rawData[byteIndex+3]>0) rawData[byteIndex+3] = (char)(alpha * 255);
        rawData[byteIndex+3] = (char)(alpha * rawData[byteIndex+3]);
        
        byteIndex += 4;
    }
    
    //create new image
    CGContextRef ctx;
    ctx = CGBitmapContextCreate(rawData,
                                CGImageGetWidth(imageRef),
                                CGImageGetHeight(imageRef),
                                bitsPercomponent,
                                CGImageGetBytesPerRow(imageRef),
                                CGImageGetColorSpace(imageRef),
                                (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    imageRef = CGBitmapContextCreateImage(ctx);
    UIImage *renderedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGContextRelease(ctx);
    free(rawData);
    
    return renderedImage;
}


#pragma mark -
#pragma mark date


#pragma mark -
#pragma mark method


#pragma mark -
#pragma mark animation


#pragma mark -
#pragma mark system


#pragma mark -
#pragma mark device


#pragma mark -
#pragma mark storyboard
+ (id)controllerWithIdentifier:(NSString *)identifier {
    return [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:identifier];
}

#pragma mark -
#pragma mark nib
+ (id)controllerWithNib:(NSString *)nib {
    Class aController = NSClassFromString(nib);
    if (![aController isSubclassOfClass:[UIViewController class]]) {
        return nil;
    }
    
    return [[aController alloc] initWithNibName:nib bundle:nil];
}

#pragma mark -
#pragma mark navigation
+ (void)gotoURL:(NSString *)str {
    if (str) {
        NSURL *url = [NSURL URLWithString:str];
        
        [[UIApplication sharedApplication] openURL:url];
    }
}
@end
