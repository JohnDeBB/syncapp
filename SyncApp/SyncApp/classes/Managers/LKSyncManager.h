//
//  LKSyncManager.h
//  SyncApp
//
//  Created by Peter Yuen on 8/14/14.
//  Copyright (c) 2014 CoSciCo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SyncFromServerTask)(NSString *content);

@interface LKSyncManager : NSObject

+ (id)sharedManager;

//- (void)syncToSever:(NSString *)content onSuccess:(CallbackTask)succeed onFailure:(CallbackTask)failed;
//- (void)syncFromSever:(NSString *)content onSuccess:(CallbackTask)succeed onFailure:(CallbackTask)failed;


- (void)syncToSeverWithContent:(NSString *)content;
- (void)registerContentSyncFromServerTask:(SyncFromServerTask)task;
- (void)syncFromSeverImmediately;

- (NSString *)localNoteContent;


@end
