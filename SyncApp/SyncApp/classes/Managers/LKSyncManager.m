//
//  LKSyncManager.m
//  SyncApp
//
//  Created by Peter Yuen on 8/14/14.
//  Copyright (c) 2014 CoSciCo. All rights reserved.
//

#import "LKSyncManager.h"
#import <OCCommunication.h>
#import <OCFileDto.h>



static NSString *baseURL = @"http://10.0.0.51/owncloud/remote.php/webdav";
static NSString *user = @"coscico"; //@"username";
static NSString *password = @"smartcoscico"; //@"password";
static NSString *remoteNotePath = @"documents/note.txt";
static NSString *localNotePath = @"note-local.txt";
static NSString *serverNotePath = @"note-server.txt";
//static NSString *localNoteCachePath = @"note-local-cache.txt";
static NSString *serverNoteCachePath = @"note-server-cache.txt";



@interface LKSyncManager ()
{
    NSString *localNoteContent;
    NSString *serverNoteContent;
//    NSString *localNoteContentCache;
    NSString *serverNoteContentCache;
    
    NSOperation *pushSyncOperation;
    NSOperation *pullSyncOperation;
    OCCommunication *sharedCommunication;
    
    SyncFromServerTask downSyncTask;
}
@end


@implementation LKSyncManager

CRManager(LKSyncManager);

- (id)init {
    if (self=[super init]) {
        sharedCommunication = [OCCommunication new];
        NSString *path = [Utility pathOfResourceFile:localNotePath];
        
        localNoteContent = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
        path = [Utility pathOfResourceFile:serverNoteContent];
        serverNoteContent = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
        path = [Utility pathOfResourceFile:serverNoteContentCache];
        serverNoteContentCache = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    }
    return self;
}


#pragma mark -
#pragma mark property
- (NSString *)localNoteContent {
    return [localNoteContent copy];
}

/*
- (void)syncToSever:(NSString *)content onSuccess:(CallbackTask)succeed onFailure:(CallbackTask)failed {
    
}

- (void)syncFromSever:(NSString *)content onSuccess:(CallbackTask)succeed onFailure:(CallbackTask)failed {
    
}*/


#pragma mark -
#pragma mark local version
- (BOOL)shouldSyncToServerWithContent:(NSString *)content {
    if ([content compare:localNoteContent options:NSBackwardsSearch] == NSOrderedSame) {
        return NO;
    }
    
    static NSString *contentPath = nil;
    if (!contentPath) {
        contentPath = [Utility pathOfResourceFile:localNotePath];
    }
    
    BOOL ret = [content writeToFile:contentPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    if (ret) {
        localNoteContent = content;
    }
    
    return YES;
}


- (BOOL)shouldUpdateNoteWithRemoteContent:(NSString *)content {
    if ([content compare:serverNoteContentCache options:NSBackwardsSearch] == NSOrderedSame) {
        return NO;
    }
    
    static NSString *contentPath = nil;
    if (!contentPath) {
        contentPath = [Utility pathOfResourceFile:serverNotePath];
    }
    
    BOOL ret = [content writeToFile:contentPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    if (ret) {
        serverNoteContent = content;
    }
    
    return YES;
}


- (void)commitWithContent:(NSString *)content {
    
}

- (void)updateLocalNoteWithContent:(NSString *)content {
    static NSString *contentPath = nil;
    if (!contentPath) {
        contentPath = [Utility pathOfResourceFile:localNotePath];
    }
    
    [content writeToFile:contentPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    localNoteContent = content;
}

- (void)pull {
    NSString *contentPath = [Utility pathOfResourceFile:serverNoteCachePath];
    
    static NSString *serverURL = nil;
    if (!serverURL) {
        serverURL = [baseURL stringByAppendingPathComponent:remoteNotePath];
        serverURL = [serverURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    
    [pullSyncOperation cancel];
    pullSyncOperation = nil;
    
    pullSyncOperation = [sharedCommunication downloadFile:serverURL toDestiny:contentPath withLIFOSystem:YES onCommunication:sharedCommunication progressDownload:^(NSUInteger bytesRead, long long totalBytesRead, long long totalExpectedBytesRead) {
        
    } successRequest:^(NSHTTPURLResponse *response, NSString *redirectedServer) {
        //on pull scucceed
        CRLog(@"pull from server succeed...");
        NSString *remoteContent = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:nil];
        if ([self shouldUpdateNoteWithRemoteContent:remoteContent]) {
            
            [self updateLocalNoteWithContent:remoteContent];
            
            downSyncTask(remoteContent);
        }
    } failureRequest:^(NSHTTPURLResponse *response, NSError *error) {
        CRLog(@"pull from server failed: %@...", error);
    } shouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        //Specifies that the operation should continue execution after the app has entered the background, and the expiration handler for that background task.
        [pullSyncOperation cancel];
    }];
}

- (void)pushWithContent:(NSString *)content {
    if(![self shouldSyncToServerWithContent:content]) return;
    
    NSString *contentPath = [Utility pathOfResourceFile:localNotePath];
    
    static NSString *serverURL = nil;
    if (!serverURL) {
        serverURL = [baseURL stringByAppendingPathComponent:remoteNotePath];
        serverURL = [serverURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    pushSyncOperation = nil;
    
    //Upload block
    pushSyncOperation = [sharedCommunication uploadFile:contentPath toDestiny:serverURL onCommunication:sharedCommunication progressUpload:^(NSUInteger bytesWrite, long long totalBytesWrite, long long totalExpectedBytesWrite) {
        
    } successRequest:^(NSHTTPURLResponse *response, NSString *redirectedServer) {
        CRLog(@"push to server succeed...");
    } failureRequest:^(NSHTTPURLResponse *response, NSString *redirectedServer, NSError *error) {
        CRLog(@"push to server failed: %@...", error);
    } failureBeforeRequest:^(NSError *error) {
        
    } shouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        //Specifies that the operation should continue execution after the app has entered the background, and the expiration handler for that background task.
        [pushSyncOperation cancel];
    }];
}



#pragma mark -
#pragma mark sync
- (void)registerContentSyncFromServerTask:(SyncFromServerTask)task {
    downSyncTask = [task copy];
}

- (void)syncToSeverWithContent:(NSString *)content {
    [self pushWithContent:content];
}

- (void)syncFromSeverImmediately {
    [self pull];
}

@end
